# Face moddification script

## TLDR
An attempt to create Deep Fake script.

## How does it work?
Script is based on OpenCV and its abilities to recognize faces on photos and specific points on earlier found faces.

## Dependencies
To run script you need:
 - [OpenCV](https:https://opencv.org/)
 - Python 3

## Running program
To run script you have to type in console
```bash
FaceModdification.py [Face Detection Haar Cascade] [Yaml point detection model] [Image input] [Transform Anchors] [Transform Boundary Anchors] [Photo to be transformed]
```

Where:
```
 - Face Detection Haar Cascade (Default: "haarcascade_frontalface_default.xml") Haar cascade for detecting faces.
 - Yaml point detection model (Default: "lbfmodel.yaml.txt" from "https://github.com/kurnianggoro/GSOC2017/blob/master/data/lbfmodel.yaml") Model for detecting speciffic point on detected face.
 - Image input (Default: "0") OpenCV camera input. It is numeric, script will open camera from PC. If given is string, script will search in its folder for video (or photo) with the same name.
 - Transform Anchors (Default: "transform_anchors.txt") Points' IDs clustered in triangles to allow transforms.
 - Transform Boundary Anchors (Default: "transform_anchors_around.txt") Same as earlier.
 - Photo to be transformed (Default: "Lenna.png") Imege which will be transformed.
 ```
 
## Class
For easier use, class "FaceMod" is prepared. Example use is located in "driver.py" file.

## TODO
- [ ] Repair transform anchors
- [ ] Repair bug with brightness
- [x] Add statistic function to reduce noise
- [ ] Add better statistic function to reduce noise
