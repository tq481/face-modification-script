import cv2 as cv
import numpy as np
from copy import copy
import os
import sys

class FaceMod():
	def __init__(self):
		self.previousCoords = []

	def _getFaceSize(self, landmarks):

		minx = maxx = landmarks[0][1][0][0][0][0]
		miny = maxy = landmarks[0][1][0][0][0][1]
		
		for w in range(len(landmarks[0][1][0][0])):
			if(landmarks[0][1][0][0][w][0] < minx):
				minx = landmarks[0][1][0][0][w][0]
			if(landmarks[0][1][0][0][w][0] > maxx):
				maxx = landmarks[0][1][0][0][w][0]
				
			if(landmarks[0][1][0][0][w][1] < miny):
				miny = landmarks[0][1][0][0][w][1]
			if(landmarks[0][1][0][0][w][1] > maxy):
				maxy = landmarks[0][1][0][0][w][1]
			
		return (minx,miny,maxx-minx,maxy-miny)
	
	def loadDependencies(self, faceCascadeClassifier, yamlModel, videoInput, anchors, anchorsAround, transformedSource):
		self.face_cascade = cv.CascadeClassifier(cv.data.haarcascades + faceCascadeClassifier)
		assert(not self.face_cascade.empty())
		print("a")
		self.facemark = cv.face.createFacemarkLBF()
		self.facemark.loadModel(yamlModel)
		print("b")
		try:
			self.video_capture = cv.VideoCapture(int(videoInput))
		except:
			self.video_capture = cv.VideoCapture(videoInput)
		assert(self.video_capture.isOpened())
		print("c")
		self.anchor = self._loadAnchorDependencies(anchors)
		self.anchorAround = self._loadAnchorDependencies(anchorsAround)
		print("d")
		self.imageToTransform = cv.imread(transformedSource)
		print("e")
		
		self.transformedPhotoLandmarks = self._getFaceLandmarks(self.imageToTransform)
	
	def _extractData(self, line):
		data = []

		local = ""
		for w in line:
			if(w == "#"):
				return data
			if(w != " " and w != "\n"):
				local += w
			else:
				data += [int(local) - 1]
				local = ""

		return data
	
	def _loadAnchorDependencies(self, file):
		whole = []
		file = open(file,"r")
		file = file.readlines()

		for w in file:
			ret = self._extractData(w)
			if(len(ret) == 3):
				whole += [ret]

		return whole
	
	def _getFaceLandmarks(self, frame):
		#Return: Face, landmarks
		output = []
		gray_frame = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
		faces = self.face_cascade.detectMultiScale(gray_frame, 1.3, 5)
		for w in range(len(faces)):
			otp = []
			otp = [faces[w]]
			status, landmarks = self.facemark.fit(frame, faces)
			otp += [landmarks]
			output += [otp]
		return output
	
	def _smootherPoints(self, landmarks, queue = 10):
		if(len(landmarks) == 0):
			if(len(self.previousCoords) == 0):
				return []
		
		if(len(self.previousCoords) == 0):
			return landmarks
		
		out = copy(landmarks)
		
		#for w in range(len(out[0][1][0][0])):
		#	out[0][1][0][0][w][0] //= len(self.previousCoords)
		#	out[0][1][0][0][w][1] //= len(self.previousCoords)
		#
		#for w in range(len(self.previousCoords)):
		#	#multiplier = 1/(2**(len(self.previousCoords))-w)
		#	
		#	multiplier = 1/len(self.previousCoords)
		#	
		#	for e in range(len(out[0][1][0][0])):
		#		out[0][1][0][0][e][0] += (self.previousCoords[w][0][1][0][0][e][0] * multiplier)
		#		out[0][1][0][0][e][1] += (self.previousCoords[w][0][1][0][0][e][1] * multiplier)
		
		for w in range(len(out[0][1][0][0])):
			out[0][1][0][0][w][0] //= 2
			out[0][1][0][0][w][1] //= 2
		
		for w in range(len(self.previousCoords)):
			#multiplier = 1/(2**(len(self.previousCoords))-w)
			
			if(w == 0):
				multiplier = 1/(2**(len(self.previousCoords)))
			else:
				multiplier = 1/(2**(len(self.previousCoords))-w+1)
			
			for e in range(len(out[0][1][0][0])):
				out[0][1][0][0][e][0] += (self.previousCoords[w][0][1][0][0][e][0] * multiplier)
				out[0][1][0][0][e][1] += (self.previousCoords[w][0][1][0][0][e][1] * multiplier)
		
		
		if(len(self.previousCoords) >= queue):
			del(self.previousCoords[0])
		self.previousCoords += [out]
		return out
	
	def draw(self, giveNumbers = False):
		assert(self.video_capture.isOpened())
		
		ret, frame = self.video_capture.read()
		black = np.zeros((frame.shape[1],frame.shape[0],3),np.uint8)
		
		assert(ret)
		
		faceLandmarks = self._getFaceLandmarks(frame)
		faceLandmarks = self._smootherPoints(faceLandmarks)
		
		for f in range(len(faceLandmarks)):
			cv.face.drawFacemarks(black, faceLandmarks[f][1][0])

			for triangles in self.anchor:
				black = cv.line(black,(int(faceLandmarks[f][1][0][0][triangles[0]][0]),int(faceLandmarks[f][1][0][0][triangles[0]][1])),
								(int(faceLandmarks[f][1][0][0][triangles[1]][0]),int(faceLandmarks[f][1][0][0][triangles[1]][1])),(0,255,0))
				black = cv.line(black,(int(faceLandmarks[f][1][0][0][triangles[0]][0]),int(faceLandmarks[f][1][0][0][triangles[0]][1])),
								(int(faceLandmarks[f][1][0][0][triangles[2]][0]),int(faceLandmarks[f][1][0][0][triangles[2]][1])),(0,255,0))
				black = cv.line(black,(int(faceLandmarks[f][1][0][0][triangles[2]][0]),int(faceLandmarks[f][1][0][0][triangles[2]][1])),
								(int(faceLandmarks[f][1][0][0][triangles[1]][0]),int(faceLandmarks[f][1][0][0][triangles[1]][1])),(0,255,0))

			if(giveNumbers):
				for q in range(len(faceLandmarks[f][1][0][0])):
					black = cv.putText(black, str(q), (int(faceLandmarks[f][1][0][0][q][0]),int(faceLandmarks[f][1][0][0][q][1])), cv.FONT_HERSHEY_SIMPLEX,	 
						(.5), (255,255,255), 1, cv.LINE_AA) 
			frame = cv.rectangle(frame,(faceLandmarks[f][0][0],faceLandmarks[f][0][1]),
								 (faceLandmarks[f][0][0] + faceLandmarks[f][0][2],faceLandmarks[f][0][1] + faceLandmarks[f][0][3]),(0,0,255))
		
		if(len(faceLandmarks)):
			trs = self._transformateGivenImage(faceLandmarks, frame.shape[1], frame.shape[0])
		else:
			trs = np.zeros((frame.shape[0],frame.shape[1],3),np.uint8)
		return frame, trs, black #Original frame, transformed image, landmarks
	
	def _transformateGivenImage(self, faceLandmarks, shapex, shapey):
		frame = copy(self.imageToTransform)
		
		collectFrame = np.zeros((frame.shape[1],frame.shape[0],3),np.uint8)
		#print(frame.shape[0],frame.shape[1])
		if(len(faceLandmarks) == 0):
			return  cv.resize(collectFrame,(shapex,shapey))
		faceSize = self._getFaceSize(faceLandmarks)
		negativeValue = [(frame.shape[1]//2,0),(0,frame.shape[0]),(frame.shape[1],frame.shape[0]),(frame.shape[1],0),(0,0)]
		
		for triangles in self.anchor:
			booleanFrame = np.zeros((frame.shape[1],frame.shape[0],3),np.uint8)
			pt1 = (int(self.transformedPhotoLandmarks[0][1][0][0][triangles[0]][0]),int(self.transformedPhotoLandmarks[0][1][0][0][triangles[0]][1]))
			pt2 = (int(self.transformedPhotoLandmarks[0][1][0][0][triangles[1]][0]),int(self.transformedPhotoLandmarks[0][1][0][0][triangles[1]][1]))
			pt3 = (int(self.transformedPhotoLandmarks[0][1][0][0][triangles[2]][0]),int(self.transformedPhotoLandmarks[0][1][0][0][triangles[2]][1]))
			triangle_cnt = np.array( [pt1, pt2, pt3] )

			booleanFrame = cv.drawContours(booleanFrame, [triangle_cnt], 0, (255,255,255), -1)
			tmpFrame = cv.bitwise_and(frame,booleanFrame)

			pts1 = np.float32([[self.transformedPhotoLandmarks[0][1][0][0][triangles[0]][0],self.transformedPhotoLandmarks[0][1][0][0][triangles[0]][1]],
							   [self.transformedPhotoLandmarks[0][1][0][0][triangles[1]][0],self.transformedPhotoLandmarks[0][1][0][0][triangles[1]][1]],
							   [self.transformedPhotoLandmarks[0][1][0][0][triangles[2]][0],self.transformedPhotoLandmarks[0][1][0][0][triangles[2]][1]]])
			pts2 = np.float32([[faceLandmarks[0][1][0][0][triangles[0]][0],faceLandmarks[0][1][0][0][triangles[0]][1]],
							   [faceLandmarks[0][1][0][0][triangles[1]][0],faceLandmarks[0][1][0][0][triangles[1]][1]],
							   [faceLandmarks[0][1][0][0][triangles[2]][0],faceLandmarks[0][1][0][0][triangles[2]][1]]])

			M = cv.getAffineTransform(pts1,pts2)

			tmpFrame = cv.warpAffine(tmpFrame,M,(frame.shape[1],frame.shape[0]))
			#print(tmpFrame.shape[0],tmpFrame.shape[1])
			collectFrame = cv.add(collectFrame,tmpFrame)

		for triangles in self.anchorAround:

			booleanFrame = np.zeros((frame.shape[1],frame.shape[0],3),np.uint8)
			if(triangles[0] >= 0):
				pt1 = (int(self.transformedPhotoLandmarks[0][1][0][0][triangles[0]][0]),int(self.transformedPhotoLandmarks[0][1][0][0][triangles[0]][1]))
			else:
				pt1 = negativeValue[triangles[0]+5]

			if(triangles[1] >= 0):
				pt2 = (int(self.transformedPhotoLandmarks[0][1][0][0][triangles[1]][0]),int(self.transformedPhotoLandmarks[0][1][0][0][triangles[1]][1]))
			else:
				pt2 = negativeValue[triangles[1]+5]
			if(triangles[2] >= 0):
				pt3 = (int(self.transformedPhotoLandmarks[0][1][0][0][triangles[2]][0]),int(self.transformedPhotoLandmarks[0][1][0][0][triangles[2]][1]))
			else:
				pt3 = negativeValue[triangles[2]+5]
			triangle_cnt = np.array( [pt1, pt2, pt3] )

			booleanFrame = cv.drawContours(booleanFrame, [triangle_cnt], 0, (255,255,255), -1)

			tmpFrame = cv.bitwise_and(frame,booleanFrame)

			pts1 = np.float32([pt1,pt2,pt3])

			if(triangles[0] >= 0):
				pt1 = (int(faceLandmarks[0][1][0][0][triangles[0]][0]),int(faceLandmarks[0][1][0][0][triangles[0]][1]))
			else:
				pt1 = negativeValue[triangles[0]+5]
			if(triangles[1] >= 0):
				pt2 = (int(faceLandmarks[0][1][0][0][triangles[1]][0]),int(faceLandmarks[0][1][0][0][triangles[1]][1]))
			else:
				pt2 = negativeValue[triangles[1]+5]
			if(triangles[2] >= 0):
				pt3 = (int(faceLandmarks[0][1][0][0][triangles[2]][0]),int(faceLandmarks[0][1][0][0][triangles[2]][1]))
			else:
				pt3 = negativeValue[triangles[2]+5]

			pts2 = np.float32([pt1,pt2,pt3])

			M = cv.getAffineTransform(pts1,pts2)

			tmpFrame = cv.warpAffine(tmpFrame,M,(frame.shape[1],frame.shape[1]))
			collectFrame = cv.add(collectFrame,tmpFrame)
		
		return cv.resize(collectFrame,(shapex,shapey))