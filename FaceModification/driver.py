#Made by Jakub Olszewski 2021
from refactoredFaceModification import FaceMod
import cv2 as cv

if __name__ == "__main__":
	a = FaceMod()
	a.loadDependencies("haarcascade_frontalface_default.xml","lbfmodel.yaml.txt",0,"transform_anchors.txt","transform_anchors_around.txt","Lenna.png")
	
	print("Done")
	
	while(True):
		b = a.draw(True)
		cv.imshow("Frame",b[0])
		if(b[1].any()): cv.imshow("Transformed",b[1])
		cv.imshow("Landmarks",b[2])
		if cv.waitKey(1) & 0xFF == 27:
			break