import cv2 as cv
import numpy as np
from copy import copy
import os
import sys

def extractData(line):
    data = []

    local = ""
    for w in line:
        if(w == "#"):
            return data
        if(w != " " and w != "\n"):
            local += w
        else:
            data += [int(local) - 1]
            local = ""

    return data

def loadAnchorDependencies(file):
    whole = []
    file = open(file,"r")
    file = file.readlines()

    for w in file:
        ret = extractData(w)
        if(len(ret) == 3):
            whole += [ret]

    return whole

def draw(video_capture, dependencies, aroundDependencies, face_cascade, facemark_cascade, transformedPhoto, giveNumbers=False):
    while(video_capture.isOpened()):
        ret, frame = video_capture.read()
        black = np.zeros((frame.shape[0],frame.shape[1],3),np.uint8)

        if ret :
            faceLandmarks = getFaceLandmarks(frame,face_cascade,facemark_cascade)

            for f in range(len(faceLandmarks)):
                cv.face.drawFacemarks(black, faceLandmarks[f][1][0])

                for triangles in dependencies:
                    black = cv.line(black,(int(faceLandmarks[f][1][0][0][triangles[0]][0]),int(faceLandmarks[f][1][0][0][triangles[0]][1])),
                                    (int(faceLandmarks[f][1][0][0][triangles[1]][0]),int(faceLandmarks[f][1][0][0][triangles[1]][1])),(0,255,0))
                    black = cv.line(black,(int(faceLandmarks[f][1][0][0][triangles[0]][0]),int(faceLandmarks[f][1][0][0][triangles[0]][1])),
                                    (int(faceLandmarks[f][1][0][0][triangles[2]][0]),int(faceLandmarks[f][1][0][0][triangles[2]][1])),(0,255,0))
                    black = cv.line(black,(int(faceLandmarks[f][1][0][0][triangles[2]][0]),int(faceLandmarks[f][1][0][0][triangles[2]][1])),
                                    (int(faceLandmarks[f][1][0][0][triangles[1]][0]),int(faceLandmarks[f][1][0][0][triangles[1]][1])),(0,255,0))
                
                #for triangles in aroundDependencies:
                #    black = cv.line(black,(int(faceLandmarks[f][1][0][0][triangles[0]][0]),int(faceLandmarks[f][1][0][0][triangles[0]][1])),
                #                    (int(faceLandmarks[f][1][0][0][triangles[1]][0]),int(faceLandmarks[f][1][0][0][triangles[1]][1])),(255,255,0))
                #    black = cv.line(black,(int(faceLandmarks[f][1][0][0][triangles[0]][0]),int(faceLandmarks[f][1][0][0][triangles[0]][1])),
                #                    (int(faceLandmarks[f][1][0][0][triangles[2]][0]),int(faceLandmarks[f][1][0][0][triangles[2]][1])),(255,255,0))
                #    black = cv.line(black,(int(faceLandmarks[f][1][0][0][triangles[2]][0]),int(faceLandmarks[f][1][0][0][triangles[2]][1])),
                #                    (int(faceLandmarks[f][1][0][0][triangles[1]][0]),int(faceLandmarks[f][1][0][0][triangles[1]][1])),(255,255,0))

                #for a in range(0,8):
                #    black = cv.line(black,(0,black.shape[1]),
                #                    (int(faceLandmarks[f][1][0][0][a][0]),int(faceLandmarks[f][1][0][0][a][1])),(0,255,255))
                #for a in range(8,17):
                #    black = cv.line(black,(black.shape[0],black.shape[1]),
                #                    (int(faceLandmarks[f][1][0][0][a][0]),int(faceLandmarks[f][1][0][0][a][1])),(0,255,255))
                #for a in [16,24,25,26]:
                #    black = cv.line(black,(black.shape[0],0),
                #                    (int(faceLandmarks[f][1][0][0][a][0]),int(faceLandmarks[f][1][0][0][a][1])),(0,255,255))
                #for a in range(19,25):
                #    black = cv.line(black,(black.shape[0] // 2,0),
                #                    (int(faceLandmarks[f][1][0][0][a][0]),int(faceLandmarks[f][1][0][0][a][1])),(0,255,255))
                #for a in [0,17,18,19]:
                #    black = cv.line(black,(0,0),
                #                    (int(faceLandmarks[f][1][0][0][a][0]),int(faceLandmarks[f][1][0][0][a][1])),(0,255,255))

                if(giveNumbers):
                    for q in range(len(faceLandmarks[f][1][0][0])):
                        black = cv.putText(black, str(q), (int(faceLandmarks[f][1][0][0][q][0]),int(faceLandmarks[f][1][0][0][q][1])), cv.FONT_HERSHEY_SIMPLEX,  
                            (.5), (255,255,255), 1, cv.LINE_AA) 
                frame = cv.rectangle(frame,(faceLandmarks[f][0][0],faceLandmarks[f][0][1]),
                                     (faceLandmarks[f][0][0] + faceLandmarks[f][0][2],faceLandmarks[f][0][1] + faceLandmarks[f][0][3]),(0,0,255))

                trs = transformateGivenImage(transformedPhoto,face_cascade,facemark_cascade,faceLandmarks,dependencies,aroundDependencies)
                cv.imshow('Transformed', trs)
            cv.imshow('Preview', frame)
            cv.imshow('Landmark_Window', black)
            if cv.waitKey(1) & 0xFF == 27:
                break
        else:
            break

    cv.destroyAllWindows()

def getFaceLandmarks(frame, face_cascade, facemark_cascade):
    #Return: Face, landmarks
    output = []
    gray_frame = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray_frame, 1.3, 5)
    for w in range(len(faces)):
        otp = []
        otp = [faces[w]]
        status, landmarks = facemark.fit(frame, faces)
        otp += [landmarks]
        output += [otp]
    return output

def transformateGivenImage(frame, face_cascade, facemark_cascade, given_points, dependencies, additional_dependencies):
    #Get coordinates from face from frame
    faceTT = getFaceLandmarks(frame, face_cascade, facemark_cascade)
    frame = copy(frame)
    finaleFrame = np.zeros((frame.shape[0],frame.shape[1],3),np.uint8)
    negativeValue = [(frame.shape[0]//2,0),(0,frame.shape[1]),(frame.shape[0],frame.shape[1]),(frame.shape[0],0),(0,0)]

    for triangles in dependencies:
        booleanFrame = np.zeros((frame.shape[0],frame.shape[1],3),np.uint8)
        pt1 = (int(faceTT[0][1][0][0][triangles[0]][0]),int(faceTT[0][1][0][0][triangles[0]][1]))
        pt2 = (int(faceTT[0][1][0][0][triangles[1]][0]),int(faceTT[0][1][0][0][triangles[1]][1]))
        pt3 = (int(faceTT[0][1][0][0][triangles[2]][0]),int(faceTT[0][1][0][0][triangles[2]][1]))
        triangle_cnt = np.array( [pt1, pt2, pt3] )

        booleanFrame = cv.drawContours(booleanFrame, [triangle_cnt], 0, (255,255,255), -1)

        tmpFrame = cv.bitwise_and(frame,booleanFrame)

        pts1 = np.float32([[faceTT[0][1][0][0][triangles[0]][0],faceTT[0][1][0][0][triangles[0]][1]],
                           [faceTT[0][1][0][0][triangles[1]][0],faceTT[0][1][0][0][triangles[1]][1]],
                           [faceTT[0][1][0][0][triangles[2]][0],faceTT[0][1][0][0][triangles[2]][1]]])
        pts2 = np.float32([[given_points[0][1][0][0][triangles[0]][0],given_points[0][1][0][0][triangles[0]][1]],
                           [given_points[0][1][0][0][triangles[1]][0],given_points[0][1][0][0][triangles[1]][1]],
                           [given_points[0][1][0][0][triangles[2]][0],given_points[0][1][0][0][triangles[2]][1]]])

        M = cv.getAffineTransform(pts1,pts2)

        tmpFrame = cv.warpAffine(tmpFrame,M,(frame.shape[0],frame.shape[1]))
        finaleFrame = cv.add(finaleFrame,tmpFrame)

    for triangles in additional_dependencies:

        booleanFrame = np.zeros((frame.shape[0],frame.shape[1],3),np.uint8)
        if(triangles[0] >= 0):
            pt1 = (int(faceTT[0][1][0][0][triangles[0]][0]),int(faceTT[0][1][0][0][triangles[0]][1]))
        else:
            pt1 = negativeValue[triangles[0]+5]

        if(triangles[1] >= 0):
            pt2 = (int(faceTT[0][1][0][0][triangles[1]][0]),int(faceTT[0][1][0][0][triangles[1]][1]))
        else:
            pt2 = negativeValue[triangles[1]+5]
        if(triangles[2] >= 0):
            pt3 = (int(faceTT[0][1][0][0][triangles[2]][0]),int(faceTT[0][1][0][0][triangles[2]][1]))
        else:
            pt3 = negativeValue[triangles[2]+5]
        triangle_cnt = np.array( [pt1, pt2, pt3] )

        booleanFrame = cv.drawContours(booleanFrame, [triangle_cnt], 0, (255,255,255), -1)

        tmpFrame = cv.bitwise_and(frame,booleanFrame)

        pts1 = np.float32([pt1,pt2,pt3])

        if(triangles[0] >= 0):
            pt1 = (int(given_points[0][1][0][0][triangles[0]][0]),int(given_points[0][1][0][0][triangles[0]][1]))
        else:
            pt1 = negativeValue[triangles[0]+5]
        if(triangles[1] >= 0):
            pt2 = (int(given_points[0][1][0][0][triangles[1]][0]),int(given_points[0][1][0][0][triangles[1]][1]))
        else:
            pt2 = negativeValue[triangles[1]+5]
        if(triangles[2] >= 0):
            pt3 = (int(given_points[0][1][0][0][triangles[2]][0]),int(given_points[0][1][0][0][triangles[2]][1]))
        else:
            pt3 = negativeValue[triangles[2]+5]

        pts2 = np.float32([pt1,pt2,pt3])

        M = cv.getAffineTransform(pts1,pts2)

        tmpFrame = cv.warpAffine(tmpFrame,M,(frame.shape[0],frame.shape[1]))
        finaleFrame = cv.add(finaleFrame,tmpFrame)

    return finaleFrame

if __name__ == "__main__":
    try:
        assert( len(sys.argv) == 7)
    except:
        print("Wrong amount of arguments given")
        exit()

    try:
        face_cascade = cv.CascadeClassifier(cv.data.haarcascades + sys.argv[1])
        assert(not face_cascade.empty())
    except:
        print("Could not load face cascade")
        exit()
    facemark = cv.face.createFacemarkLBF()

    try:
        facemark.loadModel(sys.argv[2])
    except:
        print("Could not load point detection model")
        exit()
    
    try:
        try:
            video_capture = cv.VideoCapture(int(sys.argv[3]))
        except:
            video_capture = cv.VideoCapture(sys.argv[3])
    except:
        print("Wrong image source given")
        exit()

    try:
        anchor = loadAnchorDependencies(sys.argv[4])
    except:
        print("Could not load anchor model")
        exit()

    try:
        anchorAround = loadAnchorDependencies(sys.argv[5])
    except:
        print("Could not load boundary anchor model")
        exit()

    try:
        imageToTransform = cv.imread(sys.argv[6])
    except:
        print("Could not load photo")
        exit()

    draw(video_capture,anchor,anchorAround,face_cascade,facemark,imageToTransform)